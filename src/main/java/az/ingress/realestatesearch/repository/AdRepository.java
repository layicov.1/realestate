package az.ingress.realestatesearch.repository;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import az.ingress.realestatesearch.entity.Ad;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdRepository extends JpaRepository<Ad,Integer>, JpaSpecificationExecutor<Ad> {
    List<Ad> findAll(Specification<Ad> spec);
}
