package az.ingress.realestatesearch.spec;

import az.ingress.realestatesearch.entity.Ad;
import org.springframework.data.jpa.domain.Specification;

public class AdSpec{
    public static Specification<Ad> hasCountry(String country) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("country"), country);
    }

    public static Specification<Ad> hasAddress(String address) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("address"), address);
    }

    public static Specification<Ad> hasCost(Double cost) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("cost"), cost);
    }

    public static Specification<Ad> hasRoom(Integer room) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("room_count"), room);
    }

}