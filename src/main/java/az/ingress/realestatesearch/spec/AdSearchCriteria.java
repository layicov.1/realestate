package az.ingress.realestatesearch.spec;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdSearchCriteria {
    String country;
    String address;
    Double cost;
    Integer room_count;
}
