package az.ingress.realestatesearch.spec;

import az.ingress.realestatesearch.entity.Ad;
import org.springframework.data.jpa.domain.Specification;

public class AdSpecBuild {

        public static Specification<Ad> buildSpecification(AdSearchCriteria criteria) {
            Specification<Ad> spec = Specification.where(null);

            if (criteria.getCountry()!= null) {
                spec = spec.and(AdSpec.hasCountry(criteria.getCountry()));
            }

            if (criteria.getRoom_count() != null) {
                spec = spec.and(AdSpec.hasRoom(criteria.getRoom_count()));
            }

            if (criteria.getAddress() != null) {
                spec = spec.and(AdSpec.hasAddress(criteria.getAddress()));
            }

            if (criteria.getCost() != null) {
                spec = spec.and(AdSpec.hasCost(criteria.getCost()));
            }

            return spec;
        }

}
