package az.ingress.realestatesearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealEstateSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(RealEstateSearchApplication.class, args);
    }

}
