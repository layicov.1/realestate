package az.ingress.realestatesearch.controller;

import az.ingress.realestatesearch.dto.AdDto;
import az.ingress.realestatesearch.entity.Ad;
import az.ingress.realestatesearch.repository.AdRepository;
import az.ingress.realestatesearch.service.adService;
import az.ingress.realestatesearch.spec.AdSpec;
import az.ingress.realestatesearch.spec.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ad")
@RequiredArgsConstructor
public class AdController {
    private final adService adService;
    private final AdRepository adRepository;

    @GetMapping
    public List<AdDto> getAll() {
        return adService.allAd();
    }

    @PostMapping("/created")
    public String creat(@RequestBody AdDto adDto) {
        return adService.createAd(adDto);
    }

    @GetMapping("{id}")
    public String delete(@PathVariable Integer id) {
        return adService.deleteAd(id);
    }


    @GetMapping("/ads")
    public List<Ad> filterAds(
            @RequestParam(name = "country", required = false) String country,
            @RequestParam(name = "address", required = false) String address,
            @RequestParam(name = "room_count", required = false) Integer room_count,
            @RequestParam(name = "cost", required = false) Double cost) {

        return adService.findBooksByCriteria(country, address, cost, room_count);

    }
}
