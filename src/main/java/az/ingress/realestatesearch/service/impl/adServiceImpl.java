package az.ingress.realestatesearch.service.impl;

import az.ingress.realestatesearch.dto.AdDto;
import az.ingress.realestatesearch.entity.Ad;
import az.ingress.realestatesearch.repository.AdRepository;
import az.ingress.realestatesearch.service.adService;
import az.ingress.realestatesearch.spec.AdSearchCriteria;
import az.ingress.realestatesearch.spec.AdSpec;
import az.ingress.realestatesearch.spec.AdSpecBuild;
import az.ingress.realestatesearch.spec.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class adServiceImpl implements adService {
    private final ModelMapper modelMapper;
    private final AdRepository adRepository;

    @Override
    public String createAd(AdDto adDto) {
        adRepository.save(modelMapper.map(adDto, Ad.class));
        return "added" + adDto.getCost();
    }

    @Override
    public String deleteAd(Integer id) {
        Optional<Ad> byId = adRepository.findById(id);
        adRepository.delete(byId.get());
        return "deleted" + id;
    }

    @Override
    public String updateAd(Integer id) {
        return null;
    }

    @Override
    public List<AdDto> allAd() {
        return adRepository.findAll().stream()
                .map(ad -> {
                    AdDto adDto = new AdDto();
                    adDto.setAddress(ad.getAddress());
                    adDto.setCost(ad.getCost());
                    adDto.setRoom_count(ad.getRoom_count());
                    return adDto;
                }).collect(Collectors.toList());
    }

    public List<Ad> findBooksByCriteria(String country, String address, Double cost, Integer room_count) {
        AdSearchCriteria criteria = new AdSearchCriteria();
        criteria.setCountry(country);
        criteria.setRoom_count(room_count);
        criteria.setAddress(address);
        criteria.setCost(cost);

        Specification<Ad> spec = AdSpecBuild.buildSpecification(criteria);

        return adRepository.findAll(spec);
    }
}
