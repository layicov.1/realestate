package az.ingress.realestatesearch.service;

import az.ingress.realestatesearch.dto.AdDto;
import az.ingress.realestatesearch.entity.Ad;
import az.ingress.realestatesearch.spec.SearchCriteria;

import java.util.List;

public interface adService {
    //created
    String createAd(AdDto adDto);
    //delete
    String deleteAd(Integer id);
    //Update
    String updateAd(Integer id);
    //AllRead
    List<AdDto> allAd();
    List<Ad> findBooksByCriteria(String country, String address, Double cost,Integer room_count);
}
